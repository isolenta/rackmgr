package com.isolenta.rackmgr;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class AdminActivity extends Activity {

    private Runnable closeRunnable;
    private Handler closeHandler;

    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ListView bottomList;
    private RelativeLayout drawerRelative;
    private ActionBarDrawerToggle drawerToggle;

    private CharSequence title;
    private String[] sections;
    private String[] logout;

    private AdmStatusFragment admStatusFragment;
    private AdmControlFragment admControlFragment;
    private AdmSettingsFragment admSettingsFragment;
    private AdmLogsFragment admLogsFragment;

    private ProgressBar progressBar;

    private int currentFragmentIndex;
    private int pendingFragment;
    private boolean pendingFragmentIsLogout;
    private static long backPressedTime;

    // tick time [ms]
    private static final int idleTick = 1000;

    private int timeToClose;

    private final String TAG = this.getClass().getName();

    private final static String STATUS_FRAGMENT_TAG = "STATUS_FRAGMENT";
    private final static String CONTROL_FRAGMENT_TAG = "CONTROL_FRAGMENT";
    private final static String SETTINGS_FRAGMENT_TAG = "SETTINGS_FRAGMENT";
    private final static String LOGS_FRAGMENT_TAG = "LOGS_FRAGMENT";

    private boolean isPortrait() {
        return (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT);
    }

    public void updateIdleProgress()
    {
        timeToClose = RackApp.getPrefInt("pref_admin_timeout", 30);
        progressBar.setMax(RackApp.getPrefInt("pref_admin_timeout", 30));
        progressBar.setProgress(timeToClose);
        closeHandler.removeCallbacks(closeRunnable);
        closeHandler.postDelayed(closeRunnable, idleTick);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        backPressedTime = System.currentTimeMillis() - 2000;

        // re-configure if no configuration yet exists
        final SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefStoreName), Context.MODE_PRIVATE);
        String revision = sharedPref.getString(getString(R.string.prefKeyRevision), getString(R.string.prefValueNA));

        if (getString(R.string.prefValueNA).equals(revision)) {
            RackApp.config(this, new APICallback() {
                @Override
                void complete() {
                    if (null != admStatusFragment)
                        admStatusFragment.updateStatus();
                }

                @Override
                void error() {
                }
            });
        }

        admStatusFragment = new AdmStatusFragment();
        admControlFragment = new AdmControlFragment();
        admSettingsFragment = new AdmSettingsFragment();
        admLogsFragment = new AdmLogsFragment();

        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        title = getTitle();
        sections = getResources().getStringArray(R.array.section_names);
        TypedArray section_icons = getResources().obtainTypedArray(R.array.section_icons);

        logout = getResources().getStringArray(R.array.adm_logout);
        TypedArray logout_icon = getResources().obtainTypedArray(R.array.logout_icon);

        drawerList = (ListView)findViewById(R.id.list_drawer);
        bottomList = (ListView)findViewById(R.id.bottom_list);

        if (isPortrait()) {
            drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawerRelative = (RelativeLayout) findViewById(R.id.left_drawer);
            drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        }

        MenuItemClickListener menuItemClickListener = new MenuItemClickListener();

        drawerList.setAdapter(new ImageTextAdapter(this, R.layout.drawer_list_item, R.id.item_text, sections, section_icons));
        drawerList.setOnItemClickListener(menuItemClickListener);

        bottomList.setAdapter(new ImageTextAdapter(this, R.layout.drawer_list_item, R.id.item_text, logout, logout_icon));
        bottomList.setOnItemClickListener(menuItemClickListener);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        if (isPortrait()) {
            drawerToggle = new ActionBarDrawerToggle(
                    this,
                    drawerLayout,
                    R.drawable.ic_drawer,
                    R.string.drawer_open,
                    R.string.drawer_close
            ) {
                public void onDrawerClosed(View view) {
                    getActionBar().setTitle(title);
                    invalidateOptionsMenu();
                    if (pendingFragmentIsLogout)
                        selectLogout(pendingFragment);
                    else
                        selectItem(pendingFragment);
                }

                public void onDrawerOpened(View drawerView) {
                    getActionBar().setTitle(title);
                    invalidateOptionsMenu();
                }
            };

            drawerLayout.setDrawerListener(drawerToggle);
        }

        if (savedInstanceState != null) {
            selectItem(savedInstanceState.getInt("currentFragment"));
        } else {
            selectItem(0);
        }

        RackApp.log(this, "adm", "administrative interface opened");

        timeToClose = RackApp.getPrefInt("pref_admin_timeout", 30);
        progressBar.setMax(RackApp.getPrefInt("pref_admin_timeout", 30));
        progressBar.setProgress(timeToClose);
        progressBar.setInterpolator(this, android.R.anim.bounce_interpolator);

        // runnable for delayed activity finish
        closeRunnable = new Runnable() {
            public void run() {
                if (--timeToClose <= 0)
                    returnToUser(false);

                progressBar.setProgress(timeToClose);
                closeHandler.postDelayed(closeRunnable, idleTick);
            }
        };

        // schedule activity finishing
        closeHandler = new Handler();
        closeHandler.postDelayed(closeRunnable, idleTick);

        UpdateTouchListener updateTouch = new UpdateTouchListener();

        // update timeout while user interaction
        if (isPortrait()) {
            drawerLayout.setOnTouchListener(updateTouch);
        } else {
            drawerList.setOnTouchListener(updateTouch);
            bottomList.setOnTouchListener(updateTouch);
        }

        RackApp.config(this, null);
    }

    class UpdateTouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            // update finishing callback timeout
            updateIdleProgress();
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        AdmStatusFragment statusFragment = (AdmStatusFragment)getFragmentManager().findFragmentByTag(STATUS_FRAGMENT_TAG);

        if (null != statusFragment &&
                statusFragment.isVisible()) {

            if (backPressedTime + 2000 > System.currentTimeMillis()) {
                returnToUser(true);
            } else {
                Toast.makeText(this, getString(R.string.logoutBackPressedMessage), Toast.LENGTH_SHORT).show();
            }

            backPressedTime = System.currentTimeMillis();
        } else {
            getFragmentManager()
                    .beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                    .replace(R.id.content_frame, admStatusFragment, STATUS_FRAGMENT_TAG).commit();

            currentFragmentIndex = 0;
            drawerList.setItemChecked(0, true);
            setTitle(sections[0]);

            if (isPortrait()) {
                drawerLayout.closeDrawer(drawerRelative);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        closeHandler.removeCallbacks(closeRunnable);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("currentFragment", currentFragmentIndex);
    }

    // return back to the user activity and clear back stack
    private void returnToUser(boolean forced) {
        RackApp.log(this, "adm", "administrative interface closed " +
                        (forced?"[forced by user]":"[timeout]")
        );

        Intent intent = new Intent(this, UserActivity.class);
        startActivity(intent);
        finish();
    }

    private class MenuItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            RelativeLayout rl = (RelativeLayout)view;
            TextView t = (TextView)(rl.findViewById(R.id.item_text));

            if (isPortrait()) {
                if ((t.getText() != null) &&
                        t.getText().equals(logout[0])) {
                    pendingFragmentIsLogout = true;
                } else {
                    pendingFragmentIsLogout = false;
                }

                pendingFragment = position;

                drawerLayout.closeDrawer(drawerRelative);
            } else {
                if ((t.getText() != null) &&
                        t.getText().equals(logout[0])) {
                    selectLogout(position);
                } else {
                    selectItem(position);
                }
            }
        }
    }

    private void selectLogout(int position) {
        bottomList.setItemChecked(position, false);

        closeHandler.removeCallbacks(closeRunnable);
        returnToUser(true);
    }

    private void selectItem(int position) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);


        switch (position) {
            case 0: { // status
                fragmentTransaction.replace(R.id.content_frame, admStatusFragment, STATUS_FRAGMENT_TAG).commit();
                break;
            }

            case 1: { // control
                fragmentTransaction.replace(R.id.content_frame, admControlFragment, CONTROL_FRAGMENT_TAG).commit();
                break;
            }

            case 2: { // settings
                fragmentTransaction.replace(R.id.content_frame, admSettingsFragment, SETTINGS_FRAGMENT_TAG).commit();
                break;
            }

            case 3: { // logs
                fragmentTransaction.replace(R.id.content_frame, admLogsFragment, LOGS_FRAGMENT_TAG).commit();
                break;
            }
        }

        currentFragmentIndex = position;
        drawerList.setItemChecked(position, true);
        setTitle(sections[position]);
    }

    @Override
    public void setTitle(CharSequence _title) {
        title = _title;
        getActionBar().setTitle(title);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        if (isPortrait()) {
            drawerToggle.syncState();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (isPortrait()) {
            drawerToggle.onConfigurationChanged(newConfig);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (isPortrait() && drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        RackApp.checkUSBResult(this, data);
    }
}
