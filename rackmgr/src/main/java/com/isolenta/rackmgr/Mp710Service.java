package com.isolenta.rackmgr;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.IBinder;

import java.io.IOException;

public class Mp710Service extends Service {
    private static final String ACTION_USB_PERMISSION =
            "com.isolenta.rackmgr.USB_PERMISSION";

    private final String TAG = this.getClass().getName();

    private Mp710 mp710;

    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                synchronized (this) {
                    UsbDevice _device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if ( (_device != null) &&
                            (_device.getVendorId() == Mp710.getVendorId()) &&
                            (_device.getProductId() == Mp710.getProductId()) )
                    {
                        mp710.closeDevice();
                    }
                }
            } else if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                synchronized (this) {
                    UsbDevice _device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if ( (_device != null) &&
                            (_device.getVendorId() == Mp710.getVendorId()) &&
                            _device.getProductId() == Mp710.getProductId() )
                    {
                        mp710.requestPermission(_device, ACTION_USB_PERMISSION);
                    }
                }
            } else if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice _device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if(_device != null){
                            mp710.openDevice(_device);
                        }
                    } else {
                        mp710.closeDevice();
                    }
                }
            }
        }
    };

    public void onCreate() {
        mp710 = new Mp710(this);

        IntentFilter usbFilter = new IntentFilter();
        usbFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        usbFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        usbFilter.addAction(ACTION_USB_PERMISSION);

        // we want to listen usb events
        this.registerReceiver(mUsbReceiver, usbFilter);

        // try to enumerate
        UsbDevice dev = mp710.findDevice();
        if (dev != null)
            mp710.requestPermission(dev, ACTION_USB_PERMISSION);

        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        PendingIntent pi = intent.getParcelableExtra(Mp710Frontend.PARAM_PINTENT);
        Intent rintent = new Intent();
        int ires;
        String sres;

        try {
            switch(intent.getIntExtra(Mp710Frontend.PARAM_ID, Mp710Frontend.PI_ID_INIT)) {
                case Mp710Frontend.PI_ID_INIT:
                    rintent.putExtra(Mp710Frontend.PARAM_RESULT_CODE, Mp710Frontend.CODE_SUCCESS);
                    break;

                case Mp710Frontend.PI_ID_SET:
                    mp710.set(
                        intent.getByteExtra(Mp710Frontend.PARAM_NUM, (byte)0),
                        intent.getByteExtra(Mp710Frontend.PARAM_REG, (byte)0),
                        intent.getByteExtra(Mp710Frontend.PARAM_COM, (byte)0),
                        intent.getByteExtra(Mp710Frontend.PARAM_CMD, (byte)0),
                        intent.getByteExtra(Mp710Frontend.PARAM_PRG, (byte)0)
                    );

                    rintent.putExtra(Mp710Frontend.PARAM_RESULT_CODE, Mp710Frontend.CODE_SUCCESS);

                    break;

                case Mp710Frontend.PI_ID_GET:
                    int[] retval = new int[4];

                    mp710.get(intent.getByteExtra(Mp710Frontend.PARAM_NUM, (byte)0), retval);

                    rintent.putExtra(Mp710Frontend.PARAM_REG, retval[0]);
                    rintent.putExtra(Mp710Frontend.PARAM_COM, retval[1]);
                    rintent.putExtra(Mp710Frontend.PARAM_CMD, retval[2]);
                    rintent.putExtra(Mp710Frontend.PARAM_PRG, retval[3]);
                    rintent.putExtra(Mp710Frontend.PARAM_RESULT_CODE, Mp710Frontend.CODE_SUCCESS);

                    break;

                case Mp710Frontend.PI_ID_GET_DEV_ID:
                    ires = mp710.getDeviceId();
                    rintent.putExtra(Mp710Frontend.PARAM_INT_RESULT, ires);
                    rintent.putExtra(Mp710Frontend.PARAM_RESULT_CODE, Mp710Frontend.CODE_SUCCESS);

                    break;

                case Mp710Frontend.PI_ID_GET_SOFT_VER:
                    ires = mp710.getSoftVer();
                    rintent.putExtra(Mp710Frontend.PARAM_INT_RESULT, ires);
                    rintent.putExtra(Mp710Frontend.PARAM_RESULT_CODE, Mp710Frontend.CODE_SUCCESS);

                    break;

                case Mp710Frontend.PI_ID_GET_DEV_FAMILY:
                    ires = mp710.getDeviceFamily();
                    rintent.putExtra(Mp710Frontend.PARAM_INT_RESULT, ires);
                    rintent.putExtra(Mp710Frontend.PARAM_RESULT_CODE, Mp710Frontend.CODE_SUCCESS);

                    break;

                case Mp710Frontend.PI_ID_GET_MANUFACTURER:
                    sres = mp710.getManufacturer();
                    rintent.putExtra(Mp710Frontend.PARAM_STRING_RESULT, sres);
                    rintent.putExtra(Mp710Frontend.PARAM_RESULT_CODE, Mp710Frontend.CODE_SUCCESS);

                    break;

                case Mp710Frontend.PI_ID_GET_PRODUCT:
                    sres = mp710.getProduct();
                    rintent.putExtra(Mp710Frontend.PARAM_STRING_RESULT, sres);
                    rintent.putExtra(Mp710Frontend.PARAM_RESULT_CODE, Mp710Frontend.CODE_SUCCESS);

                    break;
            } // switch
        } catch (IOException ex) {
            rintent.putExtra(Mp710Frontend.PARAM_RESULT_CODE, Mp710Frontend.CODE_ERROR);
            rintent.putExtra(Mp710Frontend.PARAM_CODE_STRING, ex.getMessage());
        }

        try {
            if (pi != null)
                pi.send(this, Mp710Frontend.STATUS_FINISH, rintent);
        }
        catch (PendingIntent.CanceledException ex) {
        }

        return super.onStartCommand(intent, flags, startId);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
