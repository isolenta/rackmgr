package com.isolenta.rackmgr;

import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.List;

public class APIParams {
    public static final int API_METHOD_GET = 0;
    public static final int API_METHOD_PUT = 1;
    public static final int API_METHOD_POST = 2;

    public String url = "";
    public int method = API_METHOD_GET;

    public String endpoint = "";
    public List<NameValuePair> reqParams;

    public APIParams() {
        reqParams = new ArrayList<NameValuePair>();
    }

    public APIParams(APIParams p) {
        this.method = p.method;
        this.endpoint = p.endpoint;

        this.reqParams = new ArrayList<NameValuePair>(p.reqParams);
    }
}
