package com.isolenta.rackmgr;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

class LogItem implements Serializable {
    String timestamp;
    String scope;
    String message;

    private static final String TAG = LogItem.class.getName();

    public static final String LASTLOG_FILENAME = "lastlog";
    public static final String LOG_QUEUE_FILENAME = "log_queued";

    public LogItem() {
        scope = "global";
        message = "";
        timestamp = Long.toString(System.currentTimeMillis() / 1000);
    }

    public static ArrayList<LogItem> readLogItemsFromFile(Context ctx, String filename) {
        ArrayList<LogItem> items = new ArrayList<LogItem>();

        FileInputStream inputStream = null;
        ObjectInputStream ois = null;

        try {
            inputStream = ctx.openFileInput(filename);
            ois = new ObjectInputStream(inputStream);

            // read LogItems collection object at once
            Object object = ois.readObject();

            if (object != null)
                items.addAll( (ArrayList<LogItem>)object );

        } catch (Exception e) {
        } finally {
            try {
                if (null != ois)
                    ois.close();
                if (null != inputStream)
                    inputStream.close();
            } catch (IOException e) {}
        }

        return items;
    }

    public static void writeLogItemsToFile(Context ctx, ArrayList<LogItem> items, String filename, boolean append) {
        FileOutputStream outputStream = null;
        ObjectOutputStream oos = null;

        try {
            ArrayList<LogItem> newItems = null;

            if (append) {
                newItems = readLogItemsFromFile(ctx, filename);
                newItems.addAll(items);
            }
            else
                newItems = items;

            // truncate new collection to restricted space
            int rsize = RackApp.getPrefInt("pref_lastlog_size", 5000);
            int size = newItems.size();

            if (size > rsize) {
                newItems.subList(0, size - rsize).clear();
            }

            outputStream = ctx.openFileOutput(filename, Context.MODE_PRIVATE);
            oos = new ObjectOutputStream(outputStream);

            // write LogItem collection at once
            oos.writeObject(newItems);

        } catch (Exception e) {
        } finally {
            try {
                if (null != outputStream)
                    outputStream.close();
                if (null != oos) {
                    oos.flush();
                    oos.close();
                }
            } catch (IOException e) {}
        }
    }

    // log this item to local rolling storage
    public void logLocal(Context ctx) {
        ArrayList<LogItem> oneItemWrapped = new ArrayList<LogItem>();

        // wrap me (this) to collection
        oneItemWrapped.add(this);

        // append new record to current file
        LogItem.writeLogItemsToFile(ctx, oneItemWrapped, LASTLOG_FILENAME, true);
    }

    // return last N LogItems
    public static ArrayList<LogItem> getLastLog(Context ctx) {
        ArrayList<LogItem> items = new ArrayList<LogItem>();

        items.addAll( LogItem.readLogItemsFromFile(ctx, LASTLOG_FILENAME) );

        return items;
    }

    public String getPlainText() {
        StringBuilder item = new StringBuilder(RackUtils.formattedDate(timestamp));
        item.append(" [");
        item.append(scope);
        item.append("]: ");
        item.append(message);

        return item.toString();
    }
}
