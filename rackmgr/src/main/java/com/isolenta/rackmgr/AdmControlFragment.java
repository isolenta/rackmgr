package com.isolenta.rackmgr;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class AdmControlFragment extends Fragment {
    private final String TAG = this.getClass().getName();

    public AdmControlFragment() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.adm_control_fragment, container, false);
        ListView cellsList = (ListView)rootView.findViewById(R.id.list_cells);

        SharedPreferences sharedPref = getActivity().getSharedPreferences(getString(R.string.prefStoreName), Context.MODE_PRIVATE);

        String cellsJSON = sharedPref.getString(getString(R.string.prefKeyCells), getString(R.string.prefValueNA));
        CellAdapter cellAdapter = new CellAdapter(getActivity(), R.layout.control_list_item, Cell.cellListFromJson(cellsJSON));

        cellsList.setAdapter(cellAdapter);

        TextView emptyMessage = (TextView)rootView.findViewById(R.id.emptyMessage);
        if (cellAdapter.getCount() == 0) {
            emptyMessage.setVisibility(View.VISIBLE);
            cellsList.setVisibility(View.GONE);
        } else {
            emptyMessage.setVisibility(View.GONE);
            cellsList.setVisibility(View.VISIBLE);
        }

        cellsList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // update finishing callback timeout
                ((AdminActivity)getActivity()).updateIdleProgress();
                return false;
            }
        });

        ToggleButton btnMaintenance = (ToggleButton)rootView.findViewById(R.id.button_maintenance);
        Button btnReconfigure = (Button)rootView.findViewById(R.id.button_reconfigure);

        if (RackApp.getInstance().currentRackState == RackApp.RACK_STATE_LOCAL_MAINTENANCE)
            btnMaintenance.setChecked(false);
        else
            btnMaintenance.setChecked(true);

        btnMaintenance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // update finishing callback timeout
                ((AdminActivity)getActivity()).updateIdleProgress();

                // manual switch on/off local maintenance
                if (((ToggleButton)v).isChecked()) {
                    RackApp.getInstance().currentRackState = RackApp.RACK_STATE_NORMAL;
                    RackApp.getInstance().maintenanceReason = RackApp.MaintenanceReason.REASON_NONE;
                } else {
                    RackApp.getInstance().currentRackState = RackApp.RACK_STATE_LOCAL_MAINTENANCE;
                    RackApp.getInstance().maintenanceReason = RackApp.MaintenanceReason.REASON_MANUAL;
                }
            }
        });

        btnReconfigure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // update finishing callback timeout
                ((AdminActivity)getActivity()).updateIdleProgress();

                RackApp.config(getActivity(), new APICallback() {
                    @Override
                    void complete() {
                        Toast.makeText(getActivity(), getString(R.string.reconfigure_complete), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    void error() {
                    }
                });
            }
        });

        return rootView;
    }
}
