package com.isolenta.rackmgr;

abstract class OpenCallback extends APICallback {
    public boolean allowOpen;
    public String cell = "";
    public String dtimeExpired = "";
}
