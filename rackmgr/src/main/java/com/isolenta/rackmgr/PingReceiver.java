package com.isolenta.rackmgr;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class PingReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, PingService.class);
        context.startService(i);
    }
}
