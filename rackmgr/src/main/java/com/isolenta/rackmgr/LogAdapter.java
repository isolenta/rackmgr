package com.isolenta.rackmgr;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

public class LogAdapter extends ArrayAdapter<String> {

    private LayoutInflater mInflater;
    private int mViewResourceId;
    private int mTextViewId;
    private Context mContext;
    ArrayList<LogItem> items;

    public LogAdapter(Context context, int viewResourceId, int textViewId) {
        super(context, viewResourceId, textViewId);

        mInflater = (LayoutInflater)context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

        mViewResourceId = viewResourceId;
        mTextViewId = textViewId;
        mContext = context;

        refresh();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    public String getPlainTextItem(int position) {
        String timestamp = items.get(position).timestamp;
        String scope = items.get(position).scope;
        String message = items.get(position).message;

        StringBuilder item = new StringBuilder(RackUtils.formattedDate(timestamp));
        item.append(" [");
        item.append(scope);
        item.append("]: ");
        item.append(message);

        return item.toString();
    }

    private String getColorForHtml(int colorId) {
        return "#" + mContext.getString(colorId).substring(3);
    }

    @Override
    public String getItem(int position) {
        String timestamp = items.get(position).timestamp;
        String scope = items.get(position).scope;
        String message = items.get(position).message;

        StringBuilder item = new StringBuilder("<font color=\"");
        item.append(getColorForHtml(R.color.log_timespamp_foreground));
        item.append("\">");
        item.append(RackUtils.formattedDate(timestamp));
        item.append("</font> <font color=\"");
        item.append(getColorForHtml(R.color.log_scope_foreground));
        item.append("\">[");
        item.append(scope);
        item.append("]:</font><font color=");
        item.append(getColorForHtml(R.color.log_message_foreground));
        item.append("\">");
        item.append(message);

        return item.toString();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void refresh() {
        items = LogItem.getLastLog(mContext);
        Collections.reverse(items);

        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(mViewResourceId, null);

        TextView tv = (TextView)convertView.findViewById(mTextViewId);
        tv.setText(Html.fromHtml(getItem(position)));

        return convertView;
    }
}
