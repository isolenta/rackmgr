package com.isolenta.rackmgr;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class UserActivity extends Activity {
    private TextView userInput;
    private Mp710Frontend mp710fe;

    private View.OnClickListener keyListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button btn = (Button)v;

            if (btn.getId() == R.id.button_ent) {

                String userText = "";
                if (null != userInput.getText())
                    userText = userInput.getText().toString();

                userInput.setText( "" );

                // do not pass empty input
                if (userText.equals(""))
                    return;

                SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefStoreName), Context.MODE_PRIVATE);

                // check if code is special code: trim special char
                if (userText.charAt(0) == '*') {
                    userText = userText.substring(1);

                    if ( RackUtils.md5(userText).equals(
                            sharedPref.getString(getString(R.string.prefKeyAdminCode), getString(R.string.prefValueNA)))
                            ) {
                        // follow to admin activity
                        Intent intent = new Intent(UserActivity.this, AdminActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else if (userText.charAt(0) == '#') {
                    // master reset code follows the '#'
                    userText = userText.substring(1);

                    if ("11".equals(userText)) {
                        RackApp.followToLogin(UserActivity.this, null, true);
                    }

                    if (getString(R.string.master_reset_code).equals(userText)) {
                        RackApp.log(UserActivity.this, "user", "master reset from user scope");
                        RackApp.masterReset(UserActivity.this, true, LoginActivity.class);
                    }
                } else {
                    // normal cell open operation
                    RackApp.open(UserActivity.this, userText);
                }

            } else if (btn.getId() == R.id.button_bs) {
                if ((userInput.getText() != null) &&
                        userInput.getText().toString().length() > 0)
                    userInput.setText( userInput.getText().toString().substring(0, userInput.getText().toString().length()-1) );
            } else if (btn.getId() == R.id.button_cancel) {
                userInput.setText( "" );
            } else {
                if (btn.getText() != null)
                    userInput.setText(userInput.getText() + btn.getText().toString());
            }
        }
    };

    private final String TAG = this.getClass().getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_user);

        userInput = (TextView) findViewById(R.id.userInput);
        mp710fe = new Mp710Frontend(this);

        Button btn;

        btn = (Button)findViewById(R.id.button_0); btn.setOnClickListener(keyListener);
        btn = (Button)findViewById(R.id.button_1); btn.setOnClickListener(keyListener);
        btn = (Button)findViewById(R.id.button_2); btn.setOnClickListener(keyListener);
        btn = (Button)findViewById(R.id.button_3); btn.setOnClickListener(keyListener);
        btn = (Button)findViewById(R.id.button_4); btn.setOnClickListener(keyListener);
        btn = (Button)findViewById(R.id.button_5); btn.setOnClickListener(keyListener);
        btn = (Button)findViewById(R.id.button_6); btn.setOnClickListener(keyListener);
        btn = (Button)findViewById(R.id.button_7); btn.setOnClickListener(keyListener);
        btn = (Button)findViewById(R.id.button_8); btn.setOnClickListener(keyListener);
        btn = (Button)findViewById(R.id.button_9); btn.setOnClickListener(keyListener);
        btn = (Button)findViewById(R.id.button_cancel); btn.setOnClickListener(keyListener);
        btn = (Button)findViewById(R.id.button_bs); btn.setOnClickListener(keyListener);
        btn = (Button)findViewById(R.id.button_ent); btn.setOnClickListener(keyListener);
        btn = (Button)findViewById(R.id.button_ast); btn.setOnClickListener(keyListener);
        btn = (Button)findViewById(R.id.button_hash); btn.setOnClickListener(keyListener);

        // token: ff6a239f7457d42586e522494ab88c04 assigned: 03/05/14 11:05
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        RackApp.checkUSBResult(this, data);
    }

    public void userCodeResult(boolean allow, String errString, int cellId) {
        if (allow) {
            mp710fe.setPulse(cellId, 1000 * RackApp.getPrefInt("pref_pulse_delay", 5));
        } else {
            RackApp.showUserMessage(this, errString);
        }
    }
}
