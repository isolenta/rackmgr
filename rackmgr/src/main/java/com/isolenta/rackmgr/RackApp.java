package com.isolenta.rackmgr;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class RackApp extends Application {
    private static RackApp instance;
    private static final String TAG = RackApp.class.getName();

    public static final int RACK_STATE_NORMAL = 1;
    public static final int RACK_STATE_USB_ERROR = 2;
    public static final int RACK_STATE_LOCAL_MAINTENANCE = 3;
    public static final int RACK_STATE_ERROR = 4;
    public static final int RACK_STATE_REMOTE_MAINTENANCE = 5;

    public static final int NETWORK_HEALTH_MIN = 0;
    public static final int NETWORK_HEALTH_MAX = 10;
    public static final int NETWORK_HEALTH_THRESHOLD_DEAD = 7;
    public static final int NETWORK_HEALTH_THRESHOLD_ALIVE = 3;

    public static final int TOKEN_EXPIRE_ADVANCE_TIME = 30;

    public boolean pingHasReply = true;
    public int currentRackState = RACK_STATE_NORMAL;
    public boolean mp710Presence = false;

    public boolean pingEnabled = true;

    public enum MaintenanceReason {
        REASON_NONE,
        REASON_MANUAL,
        REASON_NETWORK
    }

    public MaintenanceReason maintenanceReason = MaintenanceReason.REASON_NONE;
    public int networkHealth = 0;

    @Override
    public final void onCreate() {
        //Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
        super.onCreate();

        // we need to trust all SSL peers
        APIRequest.trustAll();

        final SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefStoreName), Context.MODE_PRIVATE);

        // if no any configuration exists, open login activity
        String apiURL = sharedPref.getString(getString(R.string.prefKeyAPIURL), getString(R.string.prefValueNA));
        if (getString(R.string.prefValueNA).equals(apiURL)) {
            RackApp.followToLogin(this, null, false);
        }

        startService(new Intent(this, PingService.class));

        instance = this;
    }

    public static RackApp getInstance() {
        return instance;
    }

    public static void checkApiErrors(Context ctx, String callName, String errString, String errDescription) {
        if (errString.equals("invalid_scope") ||
                errString.equals("invalid_request") ||
                errString.equals("insufficient_scope") ||
                errString.equals("rack_is_not_exist") ||
                errString.equals("state_not_allowed")) {
            RackApp.log(ctx, "api", callName + ": " + errString + "(" + errDescription + ")");
            RackApp.followToLogin(ctx, ctx.getString(R.string.serviceError, errDescription), true);
        }
    }

    public static void config(final Context ctx, final APICallback cb){
        final SharedPreferences sharedPref = ctx.getSharedPreferences(ctx.getString(R.string.prefStoreName), Context.MODE_PRIVATE);

        API.setURL(sharedPref.getString(ctx.getString(R.string.prefKeyAPIURL), ctx.getString(R.string.prefValueNA)));
        API.config(ctx.getApplicationContext(),
                sharedPref.getString(ctx.getString(R.string.prefKeyAccessToken), ctx.getString(R.string.prefValueNA)),
                new ConfigCallback() {
                    @Override
                    void complete() {
                        // save configuration
                        SharedPreferences.Editor editor = sharedPref.edit();

                        editor.putString(ctx.getString(R.string.prefKeyRackID), clientId);
                        editor.putString(ctx.getString(R.string.prefKeyAdminCode), adminCode);
                        editor.putInt(ctx.getString(R.string.prefKeyRackStatus), status);
                        editor.putString(ctx.getString(R.string.prefKeyRackAddress), address);
                        editor.putString(ctx.getString(R.string.prefKeyRevision), revision);
                        editor.putString(ctx.getString(R.string.prefKeyCells), cellsJson);
                        editor.putString(ctx.getString(R.string.prefKeyReportEmail), reportEmail);

                        editor.commit();

                        if (null != cb)
                            cb.complete();
                    }

                    @Override
                    void error() {
                        if (errString.equals("invalid_token") || errString.equals("expired_token")) {
                            RackApp.silentReauth(ctx, errDescription);
                        }

                        RackApp.checkApiErrors(ctx, "config", errString, errDescription);

                        if (null != cb) {
                            cb.errString = errString;
                            cb.error();
                        }
                    }
                }
        );
    }

    public static void auth(final Context ctx, final APICallback cb) {

        final SharedPreferences sharedPref = ctx.getSharedPreferences(ctx.getString(R.string.prefStoreName), Context.MODE_PRIVATE);

        API.setURL(sharedPref.getString(ctx.getString(R.string.prefKeyAPIURL), ctx.getString(R.string.prefValueNA)));
        API.auth(ctx.getApplicationContext(),
                sharedPref.getString(ctx.getString(R.string.prefKeyAPILogin), ctx.getString(R.string.prefValueNA)),
                sharedPref.getString(ctx.getString(R.string.prefKeyAPISecret), ctx.getString(R.string.prefValueNA)),
                new AuthCallback() {
                    @Override
                    public void error() {
                        cb.errString = errString;
                        cb.error();
                    }

                    @Override
                    public void complete() {
                        SharedPreferences sharedPref = ctx.getSharedPreferences(ctx.getString(R.string.prefStoreName), Context.MODE_PRIVATE);

                        // save access token to store
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(ctx.getString(R.string.prefKeyAccessToken), accessToken);
                        editor.putString(ctx.getString(R.string.prefKeyTokenExpiresIn), expiresIn);
                        editor.putString(ctx.getString(R.string.prefKeyTokenAssignTimestamp), Long.toString(System.currentTimeMillis() / 1000));
                        editor.commit();

                        // call user handler
                        cb.complete();
                    }
                }
        );
    }

    public static void log(final Context ctx, String scope, String message) {
        final SharedPreferences sharedPref = ctx.getSharedPreferences(ctx.getString(R.string.prefStoreName), Context.MODE_PRIVATE);

        final String token = sharedPref.getString(ctx.getString(R.string.prefKeyAccessToken), ctx.getString(R.string.prefValueNA));

        // get early queued lines
        final ArrayList<LogItem> items = LogItem.readLogItemsFromFile(ctx, LogItem.LOG_QUEUE_FILENAME);

        LogItem item = new LogItem();

        item.message = message;
        item.scope = scope;

        // save to local LastLog
        item.logLocal(ctx);

        // add new string to log
        items.add(item);

        API.setURL(sharedPref.getString(ctx.getString(R.string.prefKeyAPIURL), ctx.getString(R.string.prefValueNA)));
        API.log(ctx,
                token,
                items,
                new APICallback() {
                    @Override
                    void error() {
                        if (errString.equals("invalid_token") || errString.equals("expired_token")) {
                            RackApp.silentReauth(ctx, errDescription);
                        }

                        RackApp.checkApiErrors(ctx, "log", errString, errDescription);

                        // add items to queue for further sending
                        LogItem.writeLogItemsToFile(ctx, items, LogItem.LOG_QUEUE_FILENAME, false);
                    }

                    @Override
                    public void complete() {
                        // clear log queue
                        File file = new File(ctx.getFilesDir(), LogItem.LOG_QUEUE_FILENAME);
                        file.delete();
                    }
                }
        );
    }

    public static void open(final Context ctx, final String userInput) {
        final SharedPreferences sharedPref = ctx.getSharedPreferences(ctx.getString(R.string.prefStoreName), Context.MODE_PRIVATE);

        API.setURL(sharedPref.getString(ctx.getString(R.string.prefKeyAPIURL), ctx.getString(R.string.prefValueNA)));
        API.open(ctx.getApplicationContext(),
                sharedPref.getString(ctx.getString(R.string.prefKeyAccessToken), ctx.getString(R.string.prefValueNA)),
                userInput,
                new OpenCallback() {
                    @Override
                    public void error() {
                        if (errString.equals("invalid_token") || errString.equals("expired_token")) {
                            RackApp.silentReauth(ctx, errDescription);
                        } else {
                            RackApp.log(ctx, "user", "error code entered: " + userInput);
                            RackApp.checkApiErrors(ctx, "open", errString, errDescription);
                        }

                        ((UserActivity)ctx).userCodeResult(false, errDescription, 0);
                    }

                    @Override
                    public void complete() {
                        if (allowOpen) {
                            ArrayList<Cell> cells = Cell.cellListFromJson(sharedPref.getString(ctx.getString(R.string.prefKeyCells), ""));

                            for (Cell c : cells) {
                                if (c.hash.equals( cell )) {
                                    StringBuilder msg = new StringBuilder("success code entered: ");
                                    msg.append(userInput);
                                    msg.append("; allow to open cell ");
                                    msg.append(c.channel);
                                    RackApp.log(ctx, "user", msg.toString());

                                    ((UserActivity)ctx).userCodeResult(true, "", c.channel);
                                }
                            }
                        }
                    }
                });
    }

    public static void masterResetSilent(final Context ctx,
                                         final boolean finishActivity, final Class<?> followActivity) {

        SharedPreferences sharedPref = ctx.getSharedPreferences(ctx.getString(R.string.prefStoreName), Context.MODE_PRIVATE);
        sharedPref.edit().clear().commit();

        File file = new File(ctx.getFilesDir(), LogItem.LOG_QUEUE_FILENAME);
        file.delete();

        file = new File(ctx.getFilesDir(), LogItem.LASTLOG_FILENAME);
        file.delete();

        if (finishActivity) {
            Intent intent = new Intent(ctx, followActivity);
            ctx.startActivity(intent);
            ((Activity)ctx).finish();
        }
    }

    public static void masterReset(final Context ctx,
                                          final boolean finishActivity, final Class<?> followActivity) {

        // security check: if USB plugged, we need to discard resetting
        if (RackApp.getInstance().mp710Presence) {
            RackApp.log(ctx, "adm", "discard master reset due security check (USB device presence)");
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle(ctx.getString(R.string.are_you_sure));
        builder.setMessage(ctx.getString(R.string.master_reset_confirm));

        builder.setCancelable(true);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                masterResetSilent(ctx, finishActivity, followActivity);
            }
        });

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    static int getPrefInt(String key, int defValue) {
        return Integer.parseInt(
                PreferenceManager.getDefaultSharedPreferences(RackApp.getInstance().getApplicationContext()).getString(key, Integer.toString(defValue))
        );
    }

    static String getPrefString(String key, String defValue) {
        return PreferenceManager.getDefaultSharedPreferences(RackApp.getInstance().getApplicationContext()).getString(key, defValue);
    }

    public static void followToLogin(Context from, String errorMessage, boolean sendReport) {
        Intent i = new Intent(from, LoginActivity.class);

        if (null != errorMessage)
            i.putExtra("errorMessage", errorMessage);

        i.putExtra("sendReport", sendReport);

        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        from.startActivity(i);
    }

    public static void checkUSBResult(Context context, Intent data) {
        if (data.getIntExtra(Mp710Frontend.PARAM_RESULT_CODE, Mp710Frontend.CODE_ERROR) == Mp710Frontend.CODE_ERROR) {
            String errString = data.getStringExtra(Mp710Frontend.PARAM_CODE_STRING);
            RackApp.log(context, "usb", errString);
            RackApp.getInstance().currentRackState = RackApp.RACK_STATE_USB_ERROR;

            RackApp.showUserMessage(context, context.getString(R.string.usbError, errString));
        }
    }

    public static void invalidateConfiguration(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.prefStoreName), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(context.getString(R.string.prefKeyRevision), context.getString(R.string.prefValueNA));
        editor.commit();
    }

    public static void showUserMessage(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(message);
        builder.setCancelable(true);

        final AlertDialog dlg = builder.create();
        dlg.show();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                dlg.dismiss();
                t.cancel();
            }
        }, 5000);
    }

    public static void silentReauth(final Context context, String errMessage) {
        StringBuilder msg = new StringBuilder("API call returns error: ");
        msg.append(errMessage);
        msg.append(", reauth");

        RackApp.log(context, "auth", msg.toString());
        RackApp.auth(context,
                new APICallback() {
                    @Override
                    void complete() {
                        RackApp.log(context, "auth", "auth success");
                        RackApp.config(context, null);
                    }

                    @Override
                    void error() {
                        RackApp.followToLogin(context, context.getString(R.string.error_auth_failed), false);
                    }
                }
        );
    }
}


