package com.isolenta.rackmgr;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class SqLayout extends RelativeLayout {

    public SqLayout(Context context) {
        super(context);
    }

    public SqLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SqLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec );

        int width = super.getMeasuredWidth();
        int height = super.getMeasuredHeight();

        int new_w;
        int new_h;

        if (width > height) {
            new_w = (int)((float)height * 1.35);
            new_h = height;
        } else {
            new_w = width;
            new_h = (int)((float)width * 1.35);
        }

        super.onMeasure(MeasureSpec.makeMeasureSpec(new_w , MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(new_h, MeasureSpec.EXACTLY));
    }

}
