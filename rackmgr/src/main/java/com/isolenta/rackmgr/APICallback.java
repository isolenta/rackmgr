package com.isolenta.rackmgr;

abstract class APICallback {
    protected String errString = "";
    protected String errDescription = "";

    abstract void complete();
    abstract void error();
}
