package com.isolenta.rackmgr;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;

@SuppressWarnings({"WhileLoopReplaceableByForEach", "RedundantCast"})
public class Mp710 {
    private static final int VENDOR_ID = 0x16c0;
    private static final int PRODUCT_ID = 0x05df;

    private static final int CODE_CTL_WRITE = 0x63;
    private static final int CODE_CTL_READ = 0x36;
    private static final int CODE_INFO_READ = 0x1d;

    private final String TAG = this.getClass().getName();

    private static final int FEATURE_BUF_SIZE = 8;

    private static final int STD_USB_REQUEST_GET_DESCRIPTOR = 0x06;
    private static final int LIBUSB_DT_STRING = 0x03;
    private static final int LIBUSB_REQUEST_TYPE_CLASS = (0x01 << 5);
    private static final int LIBUSB_RECIPIENT_INTERFACE = 0x01;
    private static final int LIBUSB_ENDPOINT_IN = 0x80;
    private static final int LIBUSB_ENDPOINT_OUT = 0;

    private UsbManager manager;
    private UsbDeviceConnection usbDevConn;
    private UsbDevice device;
    private Service service;

    public Mp710(Service svc){
        this.service = svc;
        this.manager = (UsbManager) service.getSystemService(Context.USB_SERVICE);
    }

    public UsbDevice findDevice() {
        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
        if (deviceList == null)
            return null;

        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();

        while(deviceIterator.hasNext()) {
            UsbDevice dev = deviceIterator.next();
            if ((dev.getVendorId() == VENDOR_ID) && (dev.getProductId() == PRODUCT_ID))
            {
                device = dev;
                return device;
            }
        }

        return null;
    }

    public static int getVendorId() {
        return VENDOR_ID;
    }

    public static int getProductId() {
        return PRODUCT_ID;
    }

    public void openDevice(UsbDevice dev) {
        device = dev;
        usbDevConn = manager.openDevice(device);

        UsbInterface intf = device.getInterface(0);
        usbDevConn.claimInterface(intf, true);
    }

    public void closeDevice() {
        device = null;
        usbDevConn = null;
    }

    public void requestPermission(UsbDevice dev, String perm) {
        PendingIntent mPermissionIntent = PendingIntent.getBroadcast(service, 0, new Intent(perm), 0);
        manager.requestPermission(dev, mPermissionIntent);
    }

    @SuppressWarnings("PointlessBitwiseExpression")
    private void sendFeatureReport(byte[] data) throws IOException {
        if (usbDevConn == null)
            throw new IOException("Device not found");

        int retval = usbDevConn.controlTransfer(LIBUSB_REQUEST_TYPE_CLASS|LIBUSB_RECIPIENT_INTERFACE|LIBUSB_ENDPOINT_OUT,
                0x09,
                (3 << 8),
                0,
                data,
                data.length,
                1000
        );

        if (retval < 0)
            throw new IOException("controlTransfer error: " + retval);
    }

    private void getFeatureReport(byte[] data) throws IOException {
        if (usbDevConn == null)
            throw new IOException("Device not found");

        int retval = usbDevConn.controlTransfer(LIBUSB_REQUEST_TYPE_CLASS|LIBUSB_RECIPIENT_INTERFACE|LIBUSB_ENDPOINT_IN,
                0x01,
                (3 << 8),
                0,
                data,
                data.length,
                1000
        );

        if (retval < 0)
            throw new IOException("controlTransfer error");
    }

    @SuppressWarnings("PointlessBitwiseExpression")
    private String getString(int strIdx) throws IOException {
        String usbString;

        if (null == usbDevConn)
        {
            throw new IOException("Device not found");
        }

        UsbInterface intf = device.getInterface(0);
        usbDevConn.claimInterface(intf, true);
        byte[] rawDescs = usbDevConn.getRawDescriptors();

        try {
            byte[] buffer = new byte[255];
            int idx = rawDescs[strIdx];

            int rdo = usbDevConn.controlTransfer(UsbConstants.USB_DIR_IN
                    | UsbConstants.USB_TYPE_STANDARD, STD_USB_REQUEST_GET_DESCRIPTOR,
                    (LIBUSB_DT_STRING << 8) | idx, 0, buffer, 0xFF, 0);

            usbString = new String(buffer, 2, rdo - 2, "UTF-16LE");
        } catch (UnsupportedEncodingException e) {
            usbString = "";
        }

        return usbString;
    }

    public void set(byte num, byte reg, byte com, byte cmd, byte prg) throws IOException {
        byte[] buffer = new byte[FEATURE_BUF_SIZE];

        buffer[0] = CODE_CTL_WRITE;
        buffer[2] = reg;
        buffer[1] = num;
        buffer[3] = com;
        buffer[4] = (byte)(cmd >> 8);
        buffer[5] = (byte)(cmd & 0xff);
        buffer[6] = (byte)(prg >> 8);
        buffer[7] = (byte)(prg & 0xff);

        sendFeatureReport(buffer);
        getFeatureReport(buffer);

        if ((buffer[0] != CODE_CTL_WRITE) ||
                (buffer[1] != num) ||
                (buffer[2] != reg) ||
                (buffer[3] != com) ||
                (buffer[4] != (byte)(cmd >> 8)) ||
                (buffer[5] != (byte)(cmd & 0xff)) ||
                (buffer[6] != (byte)(prg >> 8)) ||
                (buffer[7] != (byte)(prg & 0xff))
                )
        {
            throw new IOException("mp710 I/O error");
        }
    }

    public void get(byte num, int[] retval) throws IOException {
        byte[] buffer = new byte[FEATURE_BUF_SIZE];

        buffer[0] = CODE_CTL_READ;
        buffer[1] = num;

        sendFeatureReport(buffer);
        getFeatureReport(buffer);

        if ((buffer[0] != CODE_CTL_READ) ||
                (buffer[1] != num))
        {
            throw new IOException("mp710 I/O error");
        }

        retval[0] = buffer[2];   // reg
        retval[1] = buffer[3];  // com
        retval[2] = (buffer[4] << 8) | buffer[5];    // cmd
        retval[3] = (buffer[6] << 8) | buffer[7];    // prg
    }

    public int getDeviceId() throws IOException {
        byte[] buffer = new byte[FEATURE_BUF_SIZE];

        buffer[0] = CODE_INFO_READ;
        sendFeatureReport(buffer);
        getFeatureReport(buffer);

        if (buffer[0] != CODE_INFO_READ)
            throw new IOException("mp710 I/O error");

        return (buffer[4] << 24) | (buffer[5] << 16) | (buffer[6] << 8) | buffer[7];
    }

    public int getSoftVer() throws IOException {
        byte[] buffer = new byte[FEATURE_BUF_SIZE];

        buffer[0] = CODE_INFO_READ;
        sendFeatureReport(buffer);
        getFeatureReport(buffer);

        if (buffer[0] != CODE_INFO_READ)
            throw new IOException("mp710 I/O error");

        return (buffer[3] << 8) | buffer[2];
    }

    public int getDeviceFamily() throws IOException {
        byte[] buffer = new byte[FEATURE_BUF_SIZE];

        buffer[0] = CODE_INFO_READ;
        sendFeatureReport(buffer);
        getFeatureReport(buffer);

        if (buffer[0] != CODE_INFO_READ)
            throw new IOException("mp710 I/O error");

        return buffer[1];
    }

    public String getManufacturer() throws IOException {
        return getString(14);
    }

    public String getProduct() throws IOException {
        return getString(15);
    }
}
