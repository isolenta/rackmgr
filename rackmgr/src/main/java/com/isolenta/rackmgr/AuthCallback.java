package com.isolenta.rackmgr;

abstract class AuthCallback extends APICallback {
    public String accessToken = "";
    public String expiresIn = "";
    public String refreshToken = "";
    public String scope = "";
}
