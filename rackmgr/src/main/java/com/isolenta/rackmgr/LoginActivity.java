package com.isolenta.rackmgr;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class LoginActivity extends Activity {
    private final String TAG = this.getClass().getName();

    // Values for email and password at the time of the login attempt.
    private String mLogin;
    private String mURL;
    private String mSecret;

    // UI references.
    private EditText mLoginView;
    private EditText mURLView;
    private EditText mSecretView;
    private View mLoginFormView;
    private View mLoginStatusView;
    private TextView mLoginStatusMessageView;
    private TextView mErrorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        mErrorMessage = (TextView)findViewById(R.id.error_message);
        Intent intent = getIntent();

        if (intent.getStringExtra("errorMessage") != null) {
            mErrorMessage.setText(intent.getStringExtra("errorMessage"));
            mErrorMessage.setVisibility(View.VISIBLE);
        } else {
            mErrorMessage.setVisibility(View.GONE);
        }

        final SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefStoreName), Context.MODE_PRIVATE);

        Button mSendReport = (Button)findViewById(R.id.send_report_button);

        if (intent.getBooleanExtra("sendReport", false)) {
            mSendReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StringBuilder emailBody = new StringBuilder();
                    ArrayList<LogItem> lastLog = LogItem.getLastLog(LoginActivity.this);

                    for (LogItem item: lastLog) {
                        emailBody.append(item.getPlainText());
                        emailBody.append("\r\n");
                    }

                    // send to configured report email only if specified
                    String reportEmail = sharedPref.getString(getString(R.string.prefKeyReportEmail), getString(R.string.prefValueNA));
                    if (!reportEmail.equals(getString(R.string.prefValueNA))) {
                        RackUtils.sendEmail(LoginActivity.this,
                                reportEmail,
                                "[" + sharedPref.getString(getString(R.string.prefKeyAPILogin), getString(R.string.prefValueNA)) + "] rackmgr manual report",
                                emailBody.toString()
                        );
                    }

                }
            });

            mSendReport.setVisibility(View.VISIBLE);
        } else {
            mSendReport.setVisibility(View.GONE);
        }

        if (!sharedPref.getString(getString(R.string.prefKeyAPILogin), getString(R.string.prefValueNA)).equals(getString(R.string.prefValueNA)))
            mLogin = sharedPref.getString(getString(R.string.prefKeyAPILogin), getString(R.string.prefValueNA));
        else
            mLogin = "rack_000007"; // FIXME: placeholder for debug

        mLoginView = (EditText) findViewById(R.id.api_login);
        mLoginView.setText(mLogin);

        if (!sharedPref.getString(getString(R.string.prefKeyAPIURL), getString(R.string.prefValueNA)).equals(getString(R.string.prefValueNA)))
            mURL = sharedPref.getString(getString(R.string.prefKeyAPIURL), getString(R.string.prefValueNA));
        else
            mURL = "http://racks.dev.darudar.com"; // FIXME: placeholder for debug

        mURLView = (EditText) findViewById(R.id.api_url);
        mURLView.setText(mURL);

        if (!sharedPref.getString(getString(R.string.prefKeyAPISecret), getString(R.string.prefValueNA)).equals(getString(R.string.prefValueNA)))
            mSecret = sharedPref.getString(getString(R.string.prefKeyAPISecret), getString(R.string.prefValueNA));
        else
            mSecret = "Yjc1MjFlNTdlZTBkNDJjNzFk"; // FIXME: placeholder for debug

        mSecretView = (EditText) findViewById(R.id.api_secret);
        mSecretView.setText(mSecret);

        mSecretView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mLoginStatusView = findViewById(R.id.login_status);
        mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

        findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
    }

    public void attemptLogin() {
        mLoginView.setError(null);
        mURLView.setError(null);
        mSecretView.setError(null);

        // Store values at the time of the login attempt.
        if (mLoginView.getText() != null)
            mLogin = mLoginView.getText().toString();
        else
            mLogin = "";

        if (mURLView.getText() != null)
            mURL = mURLView.getText().toString();

        if (mSecretView.getText() != null)
            mSecret = mSecretView.getText().toString();

        mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
        showProgress(true);

        StringBuilder msg = new StringBuilder("login attempt using url=");
        msg.append(mURL);
        msg.append(", login=");
        msg.append(mLogin);
        RackApp.log(this, "auth", msg.toString());

        // save entered credentials
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefStoreName), Context.MODE_PRIVATE);

        // save credentials to store
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.prefKeyAPIURL), mURL);
        editor.putString(getString(R.string.prefKeyAPILogin), mLogin);
        editor.putString(getString(R.string.prefKeyAPISecret), mSecret);
        editor.commit();

        // try to authenticate
        RackApp.auth(this, new APICallback() {
            @Override
            void complete() {
                RackApp.log(LoginActivity.this, "auth", "login success");
                mErrorMessage.setVisibility(View.GONE);

                // force to reconfigure
                RackApp.invalidateConfiguration(LoginActivity.this);

                // follow to admin activity if success login
                Intent intent = new Intent(LoginActivity.this, AdminActivity.class);
                LoginActivity.this.startActivity(intent);
                LoginActivity.this.finish();
            }

            @Override
            void error() {
                showProgress(false);

                mErrorMessage.setText(getString(R.string.error_auth_failed) + ": " + errDescription);
                mErrorMessage.setVisibility(View.VISIBLE);

                RackApp.log(LoginActivity.this, "auth", "login failed: " + errString);

                Toast.makeText(LoginActivity.this, "Auth error: " + errString, Toast.LENGTH_LONG).show();

                // remove wrong credentials from store
                SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefStoreName), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.remove(getString(R.string.prefKeyAPIURL));
                editor.remove(getString(R.string.prefKeyAPILogin));
                editor.remove(getString(R.string.prefKeyAPISecret));
                editor.commit();
            }
        });
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginStatusView.setVisibility(View.VISIBLE);
            mLoginStatusView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
                        }
                    });

            mLoginFormView.setVisibility(View.VISIBLE);
            mLoginFormView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        RackApp.getInstance().pingEnabled = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        RackApp.getInstance().pingEnabled = true;
    }
}
