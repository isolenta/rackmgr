package com.isolenta.rackmgr;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;

public class PingService extends Service {
    private final String TAG = this.getClass().getName();

    public static final String PONG_CMD_NORMAL = "1";
    public static final String PONG_CMD_RECONFIG = "2";
    public static final String PONG_CMD_REMOTE_MAINTENANCE = "3";

    private AlarmManager alarmManager;
    private PendingIntent alarmIntent;

    public void onCreate() {
        Intent i = new Intent(this, PingReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(this, 0, i, 0);
        alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * RackApp.getPrefInt("pref_ping_interval", 5), alarmIntent);

        super.onCreate();
    }

    public void onDestroy() {
        alarmManager.cancel(alarmIntent);
    }

    private boolean checkMp710DevicePresence() {
        // FIXME: assume that USB is always present
        return true;
        /*
        UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);

        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
        if (deviceList == null)
            return false;

        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();

        while(deviceIterator.hasNext()) {
            UsbDevice dev = deviceIterator.next();
            if ((dev.getVendorId() == Mp710.getVendorId()) && (dev.getProductId() == Mp710.getProductId()))
            {
                return true;
            }
        }

        return false;
        */
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        final SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefStoreName), Context.MODE_PRIVATE);

        // skip ping if disabled
        if (!RackApp.getInstance().pingEnabled)
            return START_STICKY;

        // skip ping if no configuration exists yet
        String apiURL = sharedPref.getString(getString(R.string.prefKeyAPIURL), getString(R.string.prefValueNA));
        if (getString(R.string.prefValueNA).equals(apiURL))
            return START_STICKY;

        // check USB device presence
        RackApp.getInstance().mp710Presence = checkMp710DevicePresence();
        if (!RackApp.getInstance().mp710Presence) {
            RackApp.getInstance().currentRackState = RackApp.RACK_STATE_USB_ERROR;
        }

        // check if previous PONG reply received
        if (!RackApp.getInstance().pingHasReply) {
            if (RackApp.getInstance().networkHealth < RackApp.NETWORK_HEALTH_MAX)
                RackApp.getInstance().networkHealth ++;
        }

        if (RackApp.getInstance().networkHealth >= RackApp.NETWORK_HEALTH_THRESHOLD_DEAD)
        {
            // switch to local maintenance in network crash
            RackApp.getInstance().currentRackState = RackApp.RACK_STATE_LOCAL_MAINTENANCE;
            RackApp.getInstance().maintenanceReason = RackApp.MaintenanceReason.REASON_NETWORK;
        }

        if (RackApp.getInstance().currentRackState == RackApp.RACK_STATE_LOCAL_MAINTENANCE &&
                RackApp.getInstance().networkHealth <= RackApp.NETWORK_HEALTH_THRESHOLD_ALIVE &&
                RackApp.getInstance().maintenanceReason == RackApp.MaintenanceReason.REASON_NETWORK)
        {
            // return to normal mode if network restored after crash
            RackApp.getInstance().currentRackState = RackApp.RACK_STATE_NORMAL;
            RackApp.getInstance().maintenanceReason = RackApp.MaintenanceReason.REASON_NONE;
        }

        // check if access token will be expired soon
        long expiresIn;
        long assignTimestamp;

        try {
            expiresIn = Long.parseLong(sharedPref.getString(getString(R.string.prefKeyTokenExpiresIn), getString(R.string.prefValueNA)));
            assignTimestamp = Long.parseLong(sharedPref.getString(getString(R.string.prefKeyTokenAssignTimestamp), getString(R.string.prefValueNA)));
        } catch (NumberFormatException e) {
            expiresIn = 0;
            assignTimestamp = 0;
        }

        long currentTimestamp = System.currentTimeMillis() / 1000;

        if (assignTimestamp + expiresIn - currentTimestamp <= RackApp.TOKEN_EXPIRE_ADVANCE_TIME) {
            StringBuilder msg = new StringBuilder("token ");
            msg.append(sharedPref.getString(getString(R.string.prefKeyAccessToken), getString(R.string.prefValueNA)));
            msg.append(" expired, reauth");
            RackApp.log(this, "auth", msg.toString());

            RackApp.auth(this,
                    new APICallback() {
                        @Override
                        void complete() {
                            RackApp.log(PingService.this, "auth", "auth success");
                            RackApp.config(PingService.this, null);
                        }

                        @Override
                        void error() {
                            RackApp.followToLogin(PingService.this, getString(R.string.error_auth_failed), false);
                        }
                    }
            );
        }

        // send new PING
        RackApp.getInstance().pingHasReply = false;

        API.setURL(sharedPref.getString(getString(R.string.prefKeyAPIURL), getString(R.string.prefValueNA)));
        API.ping(this,
                sharedPref.getString(getString(R.string.prefKeyAccessToken), getString(R.string.prefValueNA)),
                Integer.toString(
                        (RackApp.getInstance().currentRackState == RackApp.RACK_STATE_REMOTE_MAINTENANCE)?
                                RackApp.RACK_STATE_NORMAL:
                                RackApp.getInstance().currentRackState
                ),
                new PingCallback() {
                    @Override
                    public void error() {
                        RackApp.checkApiErrors(PingService.this, "ping", errString, errDescription);
                    }

                    @Override
                    public void complete() {
                        RackApp.getInstance().pingHasReply = true;

                        if (RackApp.getInstance().networkHealth > RackApp.NETWORK_HEALTH_MIN)
                            RackApp.getInstance().networkHealth --;

                        // force reconfigure from server
                        if (command.equals(PONG_CMD_RECONFIG))
                            RackApp.config(PingService.this, null);

                        if (command.equals(PONG_CMD_NORMAL)) {
                            // return NORMAL mode only from REMOTE MAINTENANCE
                            if (RackApp.getInstance().currentRackState == RackApp.RACK_STATE_REMOTE_MAINTENANCE)
                                RackApp.getInstance().currentRackState = RackApp.RACK_STATE_NORMAL;
                        }

                        if (command.equals(PONG_CMD_REMOTE_MAINTENANCE))
                            RackApp.getInstance().currentRackState = RackApp.RACK_STATE_REMOTE_MAINTENANCE;

                        int pongRevision = Integer.parseInt(revision);
                        int currentRevision;

                        try {
                            currentRevision = Integer.parseInt(sharedPref.getString(RackApp.getInstance().getString(R.string.prefKeyRevision), getString(R.string.prefValueNA)));
                        } catch (Exception e) {
                            currentRevision = 0;
                        }

                        // reconfigure if current revision is out of date
                        if ((pongRevision > currentRevision) && !command.equals(PONG_CMD_RECONFIG))
                            RackApp.config(PingService.this, null);
                    }
                }
        );

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }
}
