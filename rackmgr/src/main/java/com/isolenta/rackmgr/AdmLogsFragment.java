package com.isolenta.rackmgr;

import android.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.isolenta.rackmgr.support.SwipeRefreshLayout;

public class AdmLogsFragment extends Fragment {
    private final String TAG = this.getClass().getName();
    private EditText appendedText;
    private ListView logList;
    private LogAdapter logAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public AdmLogsFragment() {
        // Empty constructor required for fragment subclasses
    }

    private void appendCustomLogMessage() {
        String s = "";
        if (null != appendedText.getText())
            s = appendedText.getText().toString();

        if (null != getActivity())
            RackApp.log(getActivity().getApplicationContext(), "manual", s);

        appendedText.setText("");
        updateListView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.adm_logs_fragment, container, false);

        if (null == rootView)
            return null;

        logAdapter = new LogAdapter(getActivity(), R.layout.log_item, R.id.log_item_text);

        logList = (ListView)rootView.findViewById(R.id.list_logitems);
        logList.setAdapter(logAdapter);

        logList.setLongClickable(true);
        logList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                String text = logAdapter.getPlainTextItem(position);

                Toast.makeText(getActivity(), getString(R.string.copiedToClipboard), Toast.LENGTH_SHORT).show();
                RackUtils.copyToClipboard(getActivity(), text);

                return true;
            }
        });

        getActivity().getActionBar().setCustomView(R.layout.actionbar_refreshing);

        mSwipeRefreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.refreshLogs);
        mSwipeRefreshLayout.setColorScheme(R.color.yellow, R.color.blue, R.color.green, R.color.red);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ActionBar bar = getActivity().getActionBar();
                bar.setDisplayHomeAsUpEnabled(true);
                bar.setDisplayShowHomeEnabled(true);
                bar.setDisplayShowTitleEnabled(true);
                bar.setDisplayShowCustomEnabled(false);

                updateListView();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mSwipeRefreshLayout.setOnBeforeRefreshListener(new SwipeRefreshLayout.OnBeforeRefreshListener() {
            @Override
            public void onBeforeRefresh() {
                ActionBar bar = getActivity().getActionBar();
                bar.setDisplayHomeAsUpEnabled(false);
                bar.setDisplayShowHomeEnabled(false);
                bar.setDisplayShowTitleEnabled(false);

                bar.setDisplayShowCustomEnabled(true);
            }
        });

        mSwipeRefreshLayout.setOnGestureCancelled(new SwipeRefreshLayout.OnGestureCancelledListener() {
            @Override
            public void onCancelled() {
                ActionBar bar = getActivity().getActionBar();
                bar.setDisplayHomeAsUpEnabled(true);
                bar.setDisplayShowHomeEnabled(true);
                bar.setDisplayShowTitleEnabled(true);
                bar.setDisplayShowCustomEnabled(false);
            }
        });

        mSwipeRefreshLayout.setOverScroll(false);

        appendedText = (EditText)rootView.findViewById(R.id.appendedText);

        TextView.OnEditorActionListener enterListener = new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                ((AdminActivity)getActivity()).updateIdleProgress();

                if (null == event)
                    return false;

                if (actionId == R.id.append || actionId == EditorInfo.IME_NULL)
                {
                    appendCustomLogMessage();
                    return true;
                }

                return false;
            }
        };
        appendedText.setOnEditorActionListener(enterListener);

        ImageButton buttonAppend = (ImageButton)rootView.findViewById(R.id.buttonAppend);
        buttonAppend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendCustomLogMessage();
            }
        });

        // update close timeout while user interaction
        logList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // update finishing callback timeout
                if (null != getActivity())
                    ((AdminActivity)getActivity()).updateIdleProgress();

                return false;
            }
        });

        updateListView();

        return rootView;
    }

    public void updateListView() {
        logAdapter.refresh();

        logList.post(new Runnable() {
            @Override
            public void run() {
                logList.setSelection(0);
            }
        });
    }

}