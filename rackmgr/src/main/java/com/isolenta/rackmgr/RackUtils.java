package com.isolenta.rackmgr;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RackUtils {

    public static String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String formattedDate(String timestamp) {
        if (null == timestamp ||
                timestamp.equals(""))
            return "";

        long ts;

        try {
            ts = Long.parseLong(timestamp) * 1000;
        } catch (NumberFormatException e) {
            ts = 0;
        }

        try {
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(ts);
            return sdf.format(date);
        } catch (Exception e) {
            return "&lt;&gt;";
        }
    }

    public static void copyToClipboard(Context context, String text) {
        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("rackmgr log entry", text);
            clipboard.setPrimaryClip(clip);
        }
    }

    public static boolean isJSONValid(String json) {
        try {
            new JSONObject(json);
            return true;
        } catch (JSONException ex) {
            return false;
        }
    }

    public static String prettifyJSON(String json) {
        if (RackUtils.isJSONValid(json)) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            JsonParser parser = new JsonParser();
            JsonElement element = parser.parse(json);
            return gson.toJson(element);
        } else {
            return json;
        }
    }

    public static void sendEmail(Context context, String to, String subject, String body) {
        Intent intentEmail = new Intent(Intent.ACTION_SENDTO);

        intentEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
        intentEmail.putExtra(Intent.EXTRA_SUBJECT, subject);
        intentEmail.putExtra(Intent.EXTRA_TEXT, body);
        intentEmail.setType("message/rfc822");
        intentEmail.setData(Uri.parse("mailto:" + to));

        context.startActivity(Intent.createChooser(intentEmail, context.getString(R.string.sendEmail)));
    }
}
