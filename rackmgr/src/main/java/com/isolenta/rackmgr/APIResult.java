package com.isolenta.rackmgr;

import com.google.gson.JsonElement;

public class APIResult {
    public JsonElement jsonElement;
    public int httpCode;
    public String errString;
    public String errDescription;
    public APIRequest.APIError apiCode;

    public APIResult() {
        jsonElement = null;
    }
}
