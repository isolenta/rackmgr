package com.isolenta.rackmgr;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.apache.http.message.BasicNameValuePair;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class API {

    private static final String TAG = API.class.getName();
    private static final String ERROR_NULL = "internal NULL error";

    private static Type listType = new TypeToken<HashMap<String, String>>() {}.getType();
    private static Gson gson = new Gson();

    public static String APIUrl = "";

    public static void setURL(String url) {
        APIUrl = url;
    }

    public static void auth(final Context context, String login, String secret, final AuthCallback cb) {
        APIParams params = new APIParams();

        params.url = APIUrl;//"http://racks.dev.darudar.com";
        params.method = APIParams.API_METHOD_POST;
        params.endpoint = "auth/oauth/token/";

        params.reqParams.add(new BasicNameValuePair("client_id", login));
        params.reqParams.add(new BasicNameValuePair("client_secret", secret));
        params.reqParams.add(new BasicNameValuePair("grant_type", "none"));
        params.reqParams.add(new BasicNameValuePair("scope", "racks"));

        APIRequest ar = new APIRequest(context) {
            protected void onPostExecute(APIResult result) {
                if (result == null) {
                    cb.errString = ERROR_NULL;
                    cb.error();
                    return;
                }

                if (result.apiCode != APIError.NO_ERROR) {
                    if (null != result.errString)
                        cb.errString = result.errString;
                    else
                        cb.errString = result.apiCode.toString();

                    cb.errDescription = result.errDescription;

                    cb.error();
                    return;
                }

                try {
                    HashMap<String, String> fields = gson.fromJson(result.jsonElement, listType);

                    if (fields.containsKey("access_token"))
                        cb.accessToken = fields.get("access_token");

                    if (fields.containsKey("expires_in"))
                        cb.expiresIn = fields.get("expires_in");

                    if (fields.containsKey("refresh_token"))
                        cb.refreshToken = fields.get("refresh_token");

                    if (fields.containsKey("scope"))
                        cb.scope = fields.get("scope");

                    cb.complete();

                } catch (JsonSyntaxException e) {
                    Log.d(TAG, "JSON error: " + result.jsonElement.toString());
                    cb.errString = APIError.PARSE_ERROR.toString();
                    cb.error();
                }
            }
        };

        ar.execute(params);
    }

    public static void ping(final Context context, String oauth_token, String state, final PingCallback cb) {

        APIParams pingParams = new APIParams();

        pingParams.url = APIUrl;
        pingParams.method = APIParams.API_METHOD_POST;
        pingParams.endpoint = "racks/ping/";

        if (oauth_token == null)
            oauth_token = "";

        pingParams.reqParams.clear();
        pingParams.reqParams.add(new BasicNameValuePair("oauth_token", oauth_token));
        pingParams.reqParams.add(new BasicNameValuePair("state", state));

        APIRequest ar = new APIRequest(context) {
            protected void onPostExecute(APIResult result) {
                if (result == null) {
                    cb.errString = ERROR_NULL;
                    cb.error();
                    return;
                }

                if (result.apiCode != APIRequest.APIError.NO_ERROR) {
                    if (null != result.errString)
                        cb.errString = result.errString;
                    else
                        cb.errString = result.apiCode.toString();

                    cb.errDescription = result.errDescription;

                    cb.error();
                    return;
                }

                try {
                    HashMap<String, String> fields = gson.fromJson(result.jsonElement, listType);

                    if (fields.containsKey("revision"))
                        cb.revision = fields.get("revision");

                    if (fields.containsKey("command"))
                        cb.command = fields.get("command");

                    cb.complete();

                } catch (JsonSyntaxException e) {
                    cb.errString = APIRequest.APIError.PARSE_ERROR.toString();
                    cb.error();
                }
            }
        };

        ar.execute(pingParams);
    }

    public static void config(final Context context, String oauth_token, final ConfigCallback cb) {
        APIParams params = new APIParams();

        params.url = APIUrl;
        params.method = APIParams.API_METHOD_GET;
        params.endpoint = "racks/config/";

        if (oauth_token == null)
            oauth_token = "";

        params.reqParams.add(new BasicNameValuePair("oauth_token", oauth_token));

        APIRequest ar = new APIRequest(context) {
            protected void onPostExecute(APIResult result) {
                if (result == null) {
                    cb.errString = ERROR_NULL;
                    cb.error();
                    return;
                }

                if (result.apiCode != APIError.NO_ERROR) {
                    if (null != result.errString)
                        cb.errString = result.errString;
                    else
                        cb.errString = result.apiCode.toString();

                    cb.errDescription = result.errDescription;

                    cb.error();
                    return;
                }

                try {
                    JsonObject obj = (JsonObject)(result.jsonElement);
                    JsonObject cfg = obj.getAsJsonObject("config");

                    cb.clientId = "";
                    cb.adminCode = "";
                    cb.address = "";
                    cb.status = 0;
                    cb.revision = "";

                    if (cfg.has("client_id"))
                        cb.clientId = cfg.get("client_id").getAsString();

                    if (cfg.has("admin_code"))
                        cb.adminCode = cfg.get("admin_code").getAsString();

                    if (cfg.has("address"))
                        cb.address = cfg.get("address").getAsString();

                    if (cfg.has("status"))
                        cb.status = cfg.get("status").getAsInt();

                    if (cfg.has("revision"))
                        cb.revision = cfg.get("revision").getAsString();

                    if (cfg.has("report_email"))
                        cb.reportEmail = cfg.get("report_email").getAsString();

                    JsonElement cellsElement = cfg.get("cells");

                    if (null == cellsElement)
                        cb.cellsJson = "[]";
                    else
                        cb.cellsJson = cellsElement.toString();

                    cb.complete();

                } catch (JsonSyntaxException e) {
                    cb.errString = APIError.PARSE_ERROR.toString();
                    cb.error();
                }

            }
        };

        ar.execute(params);
    }

    public static void open(final Context context, String oauth_token, String code, final OpenCallback cb) {
        APIParams params = new APIParams();

        params.url = APIUrl;
        params.method = APIParams.API_METHOD_POST;
        params.endpoint = "rack/cell/open/";

        if (oauth_token == null)
            oauth_token = "";

        params.reqParams.add(new BasicNameValuePair("oauth_token", oauth_token));
        params.reqParams.add(new BasicNameValuePair("user_code", code));

        APIRequest ar = new APIRequest(context) {
            protected void onPostExecute(APIResult result) {
                if (result == null) {
                    cb.allowOpen = false;
                    cb.errString = ERROR_NULL;
                    cb.error();
                    return;
                }

                if (result.apiCode != APIError.NO_ERROR) {
                    if (null != result.errString)
                        cb.errString = result.errString;
                    else
                        cb.errString = result.apiCode.toString();

                    cb.errDescription = result.errDescription;

                    cb.error();
                    return;
                }

                try {
                    HashMap<String, String> fields = gson.fromJson(result.jsonElement, listType);

                    if (fields.containsKey("allow_open"))
                        cb.allowOpen = fields.get("allow_open").equals("true");
                    else
                        cb.allowOpen = false;

                    if (cb.allowOpen) {
                        if (fields.containsKey("cell"))
                            cb.cell = fields.get("cell");

                        if (fields.containsKey("dtime_expired"))
                            cb.dtimeExpired = fields.get("dtime_expired");
                    }

                    cb.complete();

                } catch (JsonSyntaxException e) {
                    cb.errString = APIError.PARSE_ERROR.toString();
                    cb.error();
                }
            }
        };

        ar.execute(params);
    }

    public static void log(final Context context, String oauth_token, ArrayList<LogItem> logItems, final APICallback cb) {
        APIParams params = new APIParams();
        final String REJECTED = "log_string_rejected";

        params.url = APIUrl;
        params.method = APIParams.API_METHOD_POST;
        params.endpoint = "racks/log/";

        if (oauth_token == null)
            oauth_token = "";

        /**
         * Log object format:
         * {
         *  "oauth_token": 12432341,
         *  "messages": {
         *    "<timestamp>" : {
         *         "scope": "<message scope (ex: open, config, ...)>",
         *         "message": "<message text>"
         *    },
         *    ...
         *  }
         * }
         * */

        params.reqParams.add(new BasicNameValuePair("oauth_token", oauth_token));

        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (LogItem logItem : logItems) {
            sb.append(logItem.timestamp);
            sb.append(": {scope:'");
            sb.append(logItem.scope);
            sb.append("',message:'");
            sb.append(logItem.message);
            sb.append("'},");
        }
        sb.append("}");

        params.reqParams.add(new BasicNameValuePair("messages", sb.toString()));

        APIRequest ar = new APIRequest(context) {
            protected void onPostExecute(APIResult result) {
                if (result == null) {
                    cb.errString = ERROR_NULL;
                    cb.error();
                    return;
                }

                if (result.apiCode != APIError.NO_ERROR) {
                    if (null != result.errString)
                        cb.errString = result.errString;
                    else
                        cb.errString = result.apiCode.toString();

                    cb.errDescription = result.errDescription;

                    cb.error();
                    return;
                }

                try {
                    HashMap<String, String> fields = gson.fromJson(result.jsonElement, listType);

                    if (fields.containsKey("accepted")) {
                        if (fields.get("accepted").equals("true")) {
                            cb.complete();
                        } else {
                            cb.errString = REJECTED;
                            cb.error();
                        }
                    }

                } catch (JsonSyntaxException e) {
                    cb.errString = APIError.PARSE_ERROR.toString();
                    cb.error();
                }
            }
        };

        ar.execute(params);
    }
}
