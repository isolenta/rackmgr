package com.isolenta.rackmgr;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;

public class ResetDialogPreference extends DialogPreference{

    public ResetDialogPreference (Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDialogClosed(boolean positive) {
        super.onDialogClosed(positive);

        if (!positive)
            return;

        RackApp.masterResetSilent(getContext(), true, LoginActivity.class);
    }
}
