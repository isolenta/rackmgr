package com.isolenta.rackmgr;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;

class Mp710Frontend {

    public static final String PARAM_ID = "com.isolenta.rackmgr.param_id";
    public static final String PARAM_REG = "com.isolenta.rackmgr.reg";
    public static final String PARAM_COM = "com.isolenta.rackmgr.com";
    public static final String PARAM_NUM = "com.isolenta.rackmgr.num";
    public static final String PARAM_CMD = "com.isolenta.rackmgr.cmd";
    public static final String PARAM_PRG = "com.isolenta.rackmgr.prg";
    public static final String PARAM_PINTENT = "com.isolenta.rackmgr.pendingIntent";
    public static final String PARAM_INT_RESULT = "com.isolenta.rackmgr.iresult";
    public static final String PARAM_STRING_RESULT = "com.isolenta.rackmgr.sresult";
    public static final String PARAM_RESULT_CODE = "com.isolenta.rackmgr.resultCode";
    public static final String PARAM_CODE_STRING = "com.isolenta.rackmgr.codeString";

    public static final int STATUS_FINISH = 200;

    public static final int CODE_SUCCESS = 0;
    public static final int CODE_ERROR = -1;

    public static final int PI_ID_INIT = -1;
    public static final int PI_ID_SET = 0;
    public static final int PI_ID_GET = 1;
    public static final int PI_ID_GET_DEV_ID = 2;
    public static final int PI_ID_GET_SOFT_VER = 3;
    public static final int PI_ID_GET_DEV_FAMILY = 4;
    public static final int PI_ID_GET_MANUFACTURER = 5;
    public static final int PI_ID_GET_PRODUCT = 6   ;

    private Activity activity;

    public Mp710Frontend(Activity act) {
        this.activity = act;

        // force service creation
        PendingIntent pi = activity.createPendingResult(PI_ID_INIT, new Intent(), 0);
        Intent intent = new Intent(activity, Mp710Service.class).putExtra(PARAM_PINTENT, pi);

        activity.startService(intent);
    }

    public void set(int num, int reg, int com, int cmd, int prg) {
        PendingIntent pi = activity.createPendingResult(PI_ID_SET, new Intent(), 0);
        Intent intent = new Intent(activity, Mp710Service.class).putExtra(PARAM_PINTENT, pi)
                .putExtra(PARAM_ID, PI_ID_SET)
                .putExtra(PARAM_NUM, (byte)num)
                .putExtra(PARAM_REG, (byte)reg)
                .putExtra(PARAM_COM, (byte)com)
                .putExtra(PARAM_CMD, (byte)cmd)
                .putExtra(PARAM_PRG, (byte)prg);

        activity.startService(intent);
    }

    public void requestRegisters(byte num) {
        PendingIntent pi = activity.createPendingResult(PI_ID_GET, new Intent(), 0);
        Intent intent = new Intent(activity, Mp710Service.class).putExtra(PARAM_PINTENT, pi)
                .putExtra(PARAM_ID, PI_ID_GET)
                .putExtra(PARAM_NUM, (byte)num);

        activity.startService(intent);
    }

    public void requestDeviceId() {
        PendingIntent pi = activity.createPendingResult(PI_ID_GET_DEV_ID, new Intent(), 0);
        Intent intent = new Intent(activity, Mp710Service.class)
                .putExtra(PARAM_ID, PI_ID_GET_DEV_ID)
                .putExtra(PARAM_PINTENT, pi);

        activity.startService(intent);
    }

    public void requestSoftVer() {
        PendingIntent pi = activity.createPendingResult(PI_ID_GET_SOFT_VER, new Intent(), 0);
        Intent intent = new Intent(activity, Mp710Service.class)
                .putExtra(PARAM_ID, PI_ID_GET_SOFT_VER)
                .putExtra(PARAM_PINTENT, pi);

        activity.startService(intent);
    }

    public void requestDeviceFamily() {
        PendingIntent pi = activity.createPendingResult(PI_ID_GET_DEV_FAMILY, new Intent(), 0);
        Intent intent = new Intent(activity, Mp710Service.class)
                .putExtra(PARAM_ID, PI_ID_GET_DEV_FAMILY)
                .putExtra(PARAM_PINTENT, pi);

        activity.startService(intent);
    }

    public void requestManufacturer() {
        PendingIntent pi = activity.createPendingResult(PI_ID_GET_MANUFACTURER, new Intent(), 0);
        Intent intent = new Intent(activity, Mp710Service.class)
                .putExtra(PARAM_ID, PI_ID_GET_MANUFACTURER)
                .putExtra(PARAM_PINTENT, pi);

        activity.startService(intent);
    }

    public void requestProduct() {
        PendingIntent pi = activity.createPendingResult(PI_ID_GET_PRODUCT, new Intent(), 0);
        Intent intent = new Intent(activity, Mp710Service.class)
                .putExtra(PARAM_ID, PI_ID_GET_PRODUCT)
                .putExtra(PARAM_PINTENT, pi);

        activity.startService(intent);
    }

    public void setPortOn(int num) {
        set(num, 127, 0, 15, 15);
    }

    public void setPortOff(int num) {
        set(num, 0, 0, 15, 15);
    }

    public void setPulse(final int portNum, final int delay) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            public Void doInBackground(Void... params) {
                setPortOn(portNum);

                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {}

                setPortOff(portNum);

                return null;
            }
        }.execute();
    }
}
