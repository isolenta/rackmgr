package com.isolenta.rackmgr;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;

public class AdmSettingsFragment extends PreferenceFragment {
    public AdmSettingsFragment() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.prefs);

        EditTextPreference prefURL = (EditTextPreference) findPreference("pref_url");

        if (null == prefURL)
            return;

        final SharedPreferences sharedPref = getActivity().getSharedPreferences(getString(R.string.prefStoreName), Context.MODE_PRIVATE);
        prefURL.setText(sharedPref.getString(getString(R.string.prefKeyAPIURL), getString(R.string.prefValueNA)));

        prefURL.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String currentValue = sharedPref.getString(getString(R.string.prefKeyAPIURL), getString(R.string.prefValueNA));

                // save new value to store
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.prefKeyAPIURL), newValue.toString());
                editor.commit();

                // reauth if URL changed
                if (!currentValue.equals(newValue.toString())) {
                    RackApp.auth(getActivity(),
                            new APICallback() {
                                @Override
                                void complete() {
                                    RackApp.log(getActivity(), "auth", "reauth success (API URL changed)");
                                    RackApp.config(getActivity(), null);
                                }

                                @Override
                                void error() {
                                    // fall back to login if invalid login data
                                    RackApp.log(getActivity(), "auth", "reauth failed (API URL changed)");

                                    RackApp.followToLogin(getActivity(), getString(R.string.need_reauth), false);
                                }
                            }
                    );
                }

                // do not save to default preference store
                return false;
            }
        });

        EditTextPreference prefLogin = (EditTextPreference) findPreference("pref_login");

        if (null == prefLogin)
            return;

        prefLogin.setText(sharedPref.getString(getString(R.string.prefKeyAPILogin), getString(R.string.prefValueNA)));

        prefLogin.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String currentValue = sharedPref.getString(getString(R.string.prefKeyAPILogin), getString(R.string.prefValueNA));

                // save new value to store
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.prefKeyAPILogin), newValue.toString());
                editor.commit();

                // reauth if login changed
                if (!currentValue.equals(newValue.toString())) {
                    RackApp.auth(getActivity(),
                            new APICallback() {
                                @Override
                                void complete() {
                                    RackApp.log(getActivity(), "auth", "reauth success (login changed)");
                                    RackApp.config(getActivity(), null);
                                }

                                @Override
                                void error() {
                                    // fall back to login if invalid login data
                                    RackApp.log(getActivity(), "auth", "reauth failed (login changed)");

                                    RackApp.followToLogin(getActivity(), getString(R.string.need_reauth), false);
                                }
                            }
                    );
                }

                // do not save to default preference store
                return false;
            }
        });
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        // update finishing callback timeout
        ((AdminActivity)getActivity()).updateIdleProgress();
        return false;
    }

}