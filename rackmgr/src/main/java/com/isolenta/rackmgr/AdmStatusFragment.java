package com.isolenta.rackmgr;

import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

import com.isolenta.rackmgr.support.SwipeRefreshLayout;

import java.util.ArrayList;

public class AdmStatusFragment extends Fragment {
    public AdmStatusFragment() {
        // Empty constructor required for fragment subclasses
    }

    private View rootView;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.adm_status_fragment, container, false);

        ScrollView scrollStatus = (ScrollView)rootView.findViewById(R.id.scrollStatus);
        scrollStatus.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // update finishing callback timeout
                ((AdminActivity)getActivity()).updateIdleProgress();
                return false;
            }
        });

        updateStatus();

        getActivity().getActionBar().setCustomView(R.layout.actionbar_refreshing);

        mSwipeRefreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.refreshStatus);
        mSwipeRefreshLayout.setColorScheme(R.color.yellow, R.color.blue, R.color.green, R.color.red);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ActionBar bar = getActivity().getActionBar();
                bar.setDisplayHomeAsUpEnabled(true);
                bar.setDisplayShowHomeEnabled(true);
                bar.setDisplayShowTitleEnabled(true);
                bar.setDisplayShowCustomEnabled(false);

                updateStatus();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mSwipeRefreshLayout.setOnBeforeRefreshListener(new SwipeRefreshLayout.OnBeforeRefreshListener() {
            @Override
            public void onBeforeRefresh() {
                ActionBar bar = getActivity().getActionBar();
                bar.setDisplayHomeAsUpEnabled(false);
                bar.setDisplayShowHomeEnabled(false);
                bar.setDisplayShowTitleEnabled(false);

                bar.setDisplayShowCustomEnabled(true);
            }
        });

        mSwipeRefreshLayout.setOnGestureCancelled(new SwipeRefreshLayout.OnGestureCancelledListener() {
            @Override
            public void onCancelled() {
                ActionBar bar = getActivity().getActionBar();
                bar.setDisplayHomeAsUpEnabled(true);
                bar.setDisplayShowHomeEnabled(true);
                bar.setDisplayShowTitleEnabled(true);
                bar.setDisplayShowCustomEnabled(false);
            }
        });

        mSwipeRefreshLayout.setOverScroll(false);

        return rootView;
    }

    public void updateStatus() {
        SharedPreferences sharedPref = getActivity().getSharedPreferences(getString(R.string.prefStoreName), Context.MODE_PRIVATE);

        TextView tvRackId = (TextView)rootView.findViewById(R.id.status_rack_id);
        TextView tvRackAddress = (TextView)rootView.findViewById(R.id.status_rack_address);
        TextView tvRackStatus = (TextView)rootView.findViewById(R.id.status_rack_status);
        TextView tvRackRevision = (TextView)rootView.findViewById(R.id.status_rack_revision);
        TextView tvNumCells = (TextView)rootView.findViewById(R.id.status_num_cells);
        TextView tvRawJSON = (TextView)rootView.findViewById(R.id.status_raw_json);
        TextView tvToken = (TextView)rootView.findViewById(R.id.status_token);
        TextView tvTokenTime = (TextView)rootView.findViewById(R.id.status_token_time);

        String[] statuses = getResources().getStringArray(R.array.rack_statuses);

        tvRackId.setText(sharedPref.getString(getString(R.string.prefKeyRackID), getString(R.string.prefValueNA)));
        tvRackAddress.setText(sharedPref.getString(getString(R.string.prefKeyRackAddress), getString(R.string.prefValueNA)));

        int status = sharedPref.getInt(getString(R.string.prefKeyRackStatus), 0);
        tvRackStatus.setText(statuses[status]);

        TableRow statusRow = (TableRow)rootView.findViewById(R.id.statusRow);
        int[] colors = getResources().getIntArray(R.array.rackColors);
        statusRow.setBackgroundColor(colors[status]);

        tvRackRevision.setText(RackUtils.formattedDate(sharedPref.getString(getString(R.string.prefKeyRevision), getString(R.string.prefValueNA))));

        String cellJSON = sharedPref.getString(getString(R.string.prefKeyCells), getString(R.string.prefValueNA));
        ArrayList<Cell> cells = Cell.cellListFromJson(cellJSON);
        int cellsTotal = cells.size();
        int cellsBusy = 0;

        for (Cell c : cells) {
            if (c.status == CellAdapter.STATUS_INUSE) {
                cellsBusy++;
            }
        }

        tvNumCells.setText(Integer.toString(cellsTotal) + " / " + Integer.toString(cellsBusy));

        tvRawJSON.setText(RackUtils.prettifyJSON(cellJSON));

        tvToken.setText(sharedPref.getString(getString(R.string.prefKeyAccessToken), getString(R.string.prefValueNA)));

        long expiresIn = 0;
        long assignTimestamp = 0;

        try {
            expiresIn = Long.parseLong(sharedPref.getString(getString(R.string.prefKeyTokenExpiresIn), getString(R.string.prefValueNA)));
            assignTimestamp = Long.parseLong(sharedPref.getString(getString(R.string.prefKeyTokenAssignTimestamp), getString(R.string.prefValueNA)));
        } catch (NumberFormatException e) {
        }

        long currentTimestamp = System.currentTimeMillis() / 1000;
        long restSec = assignTimestamp + expiresIn - currentTimestamp;
        long restMin = 0;
        if (restSec >= 60) {
            restMin = restSec / 60;
            restSec = restSec - restMin * 60;
        }

        StringBuilder tsStatusString = new StringBuilder(RackUtils.formattedDate(sharedPref.getString(getString(R.string.prefKeyTokenAssignTimestamp), getString(R.string.prefValueNA))));

        tsStatusString.append(" / ");

        if (restMin > 0) {
            tsStatusString.append(Long.toString(restMin));
            tsStatusString.append(":");
            tsStatusString.append(Long.toString(restSec));
            tsStatusString.append(" ");
            tsStatusString.append(getString(R.string.seconds));
        } else {
            tsStatusString.append(Long.toString(restSec));
            tsStatusString.append(" ");
            tsStatusString.append(getString(R.string.seconds));
        }

        tvTokenTime.setText(tsStatusString.toString());
    }
}
