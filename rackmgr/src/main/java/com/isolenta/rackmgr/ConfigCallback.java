package com.isolenta.rackmgr;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Map;

class Cell {
    public String hash;
    public int status;
    public int size;
    public int channel;

    static private Cell cellFromObject(JsonObject o){
        Cell cell = new Cell();

        if (o.has("cell"))
            cell.hash = o.get("cell").getAsString();

        if (o.has("status"))
            cell.status = o.get("status").getAsInt();

        if (o.has("size"))
            cell.size = o.get("size").getAsInt();

        if (o.has("channel"))
            cell.channel = o.get("channel").getAsInt();

        return cell;
    }

    static public ArrayList<Cell> cellListFromJson(String json) {

        ArrayList<Cell> retList = new ArrayList<Cell>();

        if (!RackUtils.isJSONValid(json))
            return retList;

        JsonParser parser = new JsonParser();
        JsonElement jsonElement = parser.parse(json);

        if (jsonElement.isJsonArray()) {
            JsonArray array = jsonElement.getAsJsonArray();

            for (JsonElement obj: array) {
                JsonObject cellObj = obj.getAsJsonObject();
                retList.add(cellFromObject(cellObj));
            }
        } else if (jsonElement.isJsonObject()) {
            JsonObject obj = (JsonObject) jsonElement;

            for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
                JsonObject cellObj = entry.getValue().getAsJsonObject();
                retList.add(cellFromObject(cellObj));
            }
        }

        return retList;
    }
}

abstract class ConfigCallback extends APICallback {
    public String clientId;
    public String adminCode;
    public int status;
    public String revision;
    public String address;
    public String reportEmail;

    public String cellsJson;
}
