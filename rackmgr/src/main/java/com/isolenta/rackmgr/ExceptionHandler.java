package com.isolenta.rackmgr;

import java.io.PrintWriter;
import java.io.StringWriter;

import static java.lang.Thread.UncaughtExceptionHandler;

public class ExceptionHandler implements
        UncaughtExceptionHandler {

    public ExceptionHandler() {
    }

    public void uncaughtException(Thread thread, Throwable exception) {
        StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));

        StringBuilder sbErrMessage = new StringBuilder("Uncaught exception: ");
        sbErrMessage.append(exception.getClass().getName());
        sbErrMessage.append(" at ");
        sbErrMessage.append(exception.getStackTrace()[0].toString());

        RackApp.log(RackApp.getInstance().getApplicationContext(),
                "fatal", sbErrMessage.toString());

        // TODO: show error activity and then restart app (exit & start again as launcher)
        System.exit(10);
    }

}
