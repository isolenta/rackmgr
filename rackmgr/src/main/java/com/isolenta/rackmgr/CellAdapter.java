package com.isolenta.rackmgr;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;

public class CellAdapter extends ArrayAdapter<String> {
    private LayoutInflater mInflater;
    private int mViewResourceId;
    private Context mContext;
    private ArrayList<Cell> mCells;
    private final String TAG = this.getClass().getName();
    private Mp710Frontend mp710fe;

    public static final int STATUS_UNKNOWN = 0;
    public static final int STATUS_TEST = 1;
    public static final int STATUS_FREEZE = 2;
    public static final int STATUS_EMPTY = 3;
    public static final int STATUS_RESERVED = 4;
    public static final int STATUS_INUSE = 5;

    public CellAdapter(Context context, int viewResourceId, ArrayList<Cell> cells) {
        super(context, viewResourceId, R.layout.control_list_item);

        mInflater = (LayoutInflater)context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

        mViewResourceId = viewResourceId;
        mContext = context;
        mCells = cells;
        mp710fe = new Mp710Frontend((Activity)mContext);
    }

    @Override
    public int getCount() {
        return mCells.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(mViewResourceId, null);

        TextView tvChannel = (TextView)convertView.findViewById(R.id.control_channel);
        TextView tvStatus = (TextView)convertView.findViewById(R.id.control_status);

        ToggleButton btnSetToggle = (ToggleButton)convertView.findViewById(R.id.button_set_toggle);
        Button btnOpen = (Button)convertView.findViewById(R.id.button_open);

        btnSetToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((ToggleButton)v).isChecked()) {
                    mp710fe.setPortOn(mCells.get(position).channel);
                } else {
                    mp710fe.setPortOff(mCells.get(position).channel);
                }
            }
        });

        btnOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp710fe.setPulse(mCells.get(position).channel, 1000 * RackApp.getPrefInt("pref_pulse_delay", 5));
            }
        });

        String[] statuses = mContext.getResources().getStringArray(R.array.cell_statuses);

        StringBuilder sbTvChannel = new StringBuilder(mContext.getString(R.string.channel));
        sbTvChannel.append(": ");
        sbTvChannel.append(Integer.toString(mCells.get(position).channel));
        tvChannel.setText(sbTvChannel.toString());

        tvStatus.setText(statuses[mCells.get(position).status]);

        LinearLayout controlItem = (LinearLayout)convertView.findViewById(R.id.controlItem);
        int[] colors = mContext.getResources().getIntArray(R.array.cellColors);
        controlItem.setBackgroundColor(colors[mCells.get(position).status]);

        return convertView;
    }
}
