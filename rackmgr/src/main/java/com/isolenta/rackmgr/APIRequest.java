package com.isolenta.rackmgr;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.apache.http.NameValuePair;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class APIRequest extends AsyncTask<APIParams, Integer, APIResult>  {

    protected Context context;

    public enum APIError {
        NO_ERROR, NETWORK_UNAVAILABLE, INVALID_URL, ENCODING_ERROR, HTTP_UNAUTHORIZED, HTTP_FORBIDDEN,
        HTTP_NOT_FOUND, HTTP_SERVER_ERROR, HTTP_OTHER_ERROR, SSL_REJECTED, IO_ERROR, OTHER_ERROR,
        PARSE_ERROR, API_ERROR, INFINITE_REDIRECT
    };

    private final String TAG = this.getClass().getName();
    private static final int MAX_REDIRECT_DEPTH = 10;
    private char[] responseBuffer;

    private APIError lastError;
    private String errString;

    public APIRequest(Context context) {
        this.context = context;

        responseBuffer = new char[256]; // TODO: 256?

        // switch off connection reuse which was buggy in pre-froyo
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO)
            System.setProperty("http.keepAlive", "false");

        lastError = APIError.NO_ERROR;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();

        return (networkInfo != null) && networkInfo.isConnected();
    }

    public void execute(APIParams params) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            super.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
        else
            super.execute(params);
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public APIResult doInBackground(APIParams... params) {

        APIResult result = new APIResult();
        String responseStr = "";

        if (!isNetworkAvailable()) {
            lastError = APIError.NETWORK_UNAVAILABLE;
            result.apiCode = lastError;
            return result;
        }

        StringBuilder sbUrl = new StringBuilder(params[0].url);
        sbUrl.append("/");
        sbUrl.append(params[0].endpoint);

        // append parameters to request as URL part
        if (params[0].method == APIParams.API_METHOD_GET) {
            try {
                sbUrl.append("?");
                sbUrl.append(getQuery(params[0].reqParams));
            } catch (UnsupportedEncodingException e) {}
        }

        // build request string
        byte[] reqData = null;
        String reqStr;
        try {
            reqStr = getQuery(params[0].reqParams);
            reqData = reqStr.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            lastError = APIError.ENCODING_ERROR;
            return null;
        }

        URL url;

        try {
            url = new URL(sbUrl.toString());
        } catch (Exception e) {
            lastError = APIError.INVALID_URL;
            result.apiCode = lastError;
            result.errString = sbUrl.toString();
            return result;
        }

        HttpURLConnection conn;

        try {
            int redirectDepth = MAX_REDIRECT_DEPTH;

            do {
                conn = (HttpURLConnection) url.openConnection();
                conn.setInstanceFollowRedirects(false);
                conn.setDoInput(true);
                conn.setUseCaches(false);

                switch (params[0].method) {
                    case APIParams.API_METHOD_POST:
                        conn.setRequestMethod("POST");
                        conn.setDoOutput(true);
                        break;

                    case APIParams.API_METHOD_PUT:
                        conn.setRequestMethod("PUT");
                        conn.setDoOutput(true);
                        break;

                    case APIParams.API_METHOD_GET:
                    default:
                        conn.setRequestMethod("GET");
                        break;
                }

                // append parameters to request as POST urlencoded part
                if (params[0].method == APIParams.API_METHOD_POST || params[0].method == APIParams.API_METHOD_PUT)
                {
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    conn.setRequestProperty("Content-Length", Integer.toString(reqData.length));

                    OutputStreamWriter out =  new OutputStreamWriter(conn.getOutputStream());
                    out.write(reqStr);
                    out.close();
                }

                // retry request if we have http redirect
                if (conn.getResponseCode() == HttpURLConnection.HTTP_MOVED_PERM ||
                        conn.getResponseCode() == HttpURLConnection.HTTP_MOVED_TEMP) {
                    try {
                        url = new URL(conn.getHeaderField("Location"));
                    } catch (Exception e) {
                        lastError = APIError.INVALID_URL;
                        result.apiCode = lastError;
                        result.errString = conn.getHeaderField("Location");
                        return result;
                    }
                }

                // detect redirection loops
                if (--redirectDepth <= 0) {
                    result.apiCode = APIError.INFINITE_REDIRECT;
                    return result;
                }
            } while (conn.getResponseCode() == HttpURLConnection.HTTP_MOVED_PERM ||
                    conn.getResponseCode() == HttpURLConnection.HTTP_MOVED_TEMP);

            int responseCode = conn.getResponseCode();

            result.httpCode = responseCode;

            switch (responseCode) {
                case HttpURLConnection.HTTP_BAD_REQUEST:
                case HttpURLConnection.HTTP_OK:
                case HttpURLConnection.HTTP_UNAUTHORIZED:
                case HttpURLConnection.HTTP_FORBIDDEN:
                    StringBuilder response = new StringBuilder();
                    InputStreamReader in;
                    if (responseCode == HttpURLConnection.HTTP_BAD_REQUEST)
                        in = new InputStreamReader(conn.getErrorStream(), "UTF-8");
                    else
                        in = new InputStreamReader(conn.getInputStream(), "UTF-8");

                    int bytes;

                    while ((bytes = in.read(responseBuffer)) != -1) {
                        response.append(responseBuffer, 0, bytes);
                    }

                    responseStr = response.toString();

                    JsonParser parser = new JsonParser();
                    result.jsonElement = parser.parse(response.toString());

                    result.apiCode = APIError.NO_ERROR;
                    result.errString = "";

                    // try to parse as json.error object
                    try {
                        Type listType = new TypeToken<HashMap<String, String>>() {}.getType();
                        HashMap<String, String> fields = new Gson().fromJson(result.jsonElement, listType);

                        if (fields.containsKey("error")) {
                            result.errString = fields.get("error");
                            result.apiCode = APIError.API_ERROR;

                            if (fields.containsKey("error_description"))
                                result.errDescription = fields.get("error_description");
                        }
                    } catch (JsonSyntaxException e) {}

                    conn.disconnect();
                    return result;

                case HttpURLConnection.HTTP_NOT_FOUND:
                    lastError = APIError.HTTP_NOT_FOUND;
                    break;
                case HttpURLConnection.HTTP_INTERNAL_ERROR:
                    lastError = APIError.HTTP_SERVER_ERROR;
                    break;
                default:
                    lastError = APIError.HTTP_OTHER_ERROR;
                    break;
            }

            errString = Integer.toString(responseCode);
            conn.disconnect();
        } catch (javax.net.ssl.SSLPeerUnverifiedException e) {
            lastError = APIError.SSL_REJECTED;
            errString = e.getMessage();
        } catch (IOException e) {
            lastError = APIError.IO_ERROR;
            errString = e.getMessage();
        } catch (Exception e) {
            lastError = APIError.OTHER_ERROR;
            StringBuilder sbErrString = new StringBuilder(e.getMessage());
            sbErrString.append(", response: ");
            sbErrString.append(responseStr);
            errString = sbErrString.toString();
        }

        result.errString = errString;
        result.apiCode = lastError;

        return result;
    }

    public static void trustAll() {
        try {
            X509TrustManager easyTrustManager = new X509TrustManager() {

                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {}

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };

            TrustManager[] trustAllCerts = new TrustManager[] { easyTrustManager };

            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (Exception e) {
        }
    }
}
